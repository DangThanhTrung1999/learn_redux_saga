module.exports = {
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
      modules: true,
      experimentalObjectRestSpread: true,
    },
  },
  extends:[
    "eslint:recommend",
    "plugin:react/recommend"
  ],
  rules: {
    semi:1,
    quotes:[2,"double"],
    "react/prop-types":1
  },
};

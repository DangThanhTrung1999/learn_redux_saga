import React from "react";
import {
  Card,
  CardContent,
  Typography,
  TextField,
  Button,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import "./style.css";
function LoginPage(props) {
  return (
    <div className="background">
      <div className="login">
        <Card>
          <CardContent>
            <form>
              <div className="text-xs-center pb-xs">
                <Typography variant="caption">Đăng nhập</Typography>
              </div>
              <TextField
                id="email"
                label="Email"
                className="textField"
                fullWidth
                margin="normal"
              />
              <TextField
                id="password"
                label="Password"
                className="textField"
                fullWidth
                type="password"
                margin="normal"
              />
              <Button
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
              >
                Login
              </Button>
              <div className="pt-1 text-md-ceneter">
                <Link to="/signup">
                  <Button>Đăng ký tài khoản mới</Button>
                </Link>
              </div>
            </form>
          </CardContent>
        </Card>
      </div>
    </div>
  );
}

export default LoginPage;

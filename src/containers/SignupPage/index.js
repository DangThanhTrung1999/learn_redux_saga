import React from "react";
import {
  Card,
  CardContent,
  Typography,
  TextField,
  Button,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import "./style.css";
function SignupPage(props) {
  return (
    <div className="background">
      <div className="signup">
        <Card>
          <CardContent>
            <form>
              <div className="text-xs-center pb-xs">
                <Typography variant="caption">Đăng nhập</Typography>
              </div>
              <TextField
                id="email"
                label="Email"
                className="textField"
                fullWidth
                margin="normal"
              />
              <TextField
                id="password"
                label="Password"
                className="textField"
                fullWidth
                type="password"
                margin="normal"
              />
              <TextField
                id="cpassword"
                label="Comfirm password"
                className="textField"
                fullWidth
                type="password"
                margin="normal"
              />
              <FormControlLabel
                control={<Checkbox value="agree" />}
                label="
                Tôi đã đọc chính sách và đồng ý điều khoản"
                className="fullWidth"
              ></FormControlLabel>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
              >
                Signup
              </Button>
              <div className="pt-1 text-md-ceneter">
                <Link to="/login">
                  <Button>Đã có tài khoản ?</Button>
                </Link>
              </div>
            </form>
          </CardContent>
        </Card>
      </div>
    </div>
  );
}

export default SignupPage;

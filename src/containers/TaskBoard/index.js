import { Button, Box } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as modalAction from "../../actions/modal";
import * as taskAction from "../../actions/task";
import { fetchListTask, filterTask } from "../../actions/task";
import SearchBox from "../../component/SearchBox";
import TaskForm from "../../component/TaskForm";
import TaskList from "../../component/TaskList";
import { STATUS } from "../../constants/index";
import "./styles.css";

function TaskBoard(props) {
  const dispatch = useDispatch();
  const listTask = useSelector((state) => state.task.listTask);
  useEffect(() => {
    dispatch(fetchListTask());
  }, []);

  const renderBoard = () => {
    let xhtml = null;
    xhtml = (
      <Grid container spacing={3}>
        {STATUS.map((status, index) => {
          let listTaskFiltered = listTask.filter(
            (task) => task.status === status.value
          );
          return (
            <TaskList
              listTaskFiltered={listTaskFiltered}
              status={status}
              key={index}
              handleEditTask={handleEditTask}
              showModalDeleteTask={showModalDeleteTask}
            />
          );
        })}
      </Grid>
    );
    return xhtml;
  };
  const handleEditTask = (task) => {
    // console.log(task);
    dispatch(taskAction.setTaskEditing(task));
    dispatch(modalAction.showModal());
    dispatch(modalAction.changeModalTitle("Cập nhật công việc"));
    dispatch(modalAction.changeModalContent(<TaskForm />));
  };
  const showModalDeleteTask = (task) => {
    dispatch(modalAction.showModal());
    dispatch(modalAction.changeModalTitle("Xóa công việc"));
    dispatch(
      modalAction.changeModalContent(
        <div style={{ padding: 20 }}>
          <div>
            Bạn có chắc chắn muốn xóa <span>{task.title}</span> ?
          </div>
          <Box display="flex" flexDirection="row-reverse" mt={2}>
            <Box ml={1}>
              <Button
                variant="contained"
                onClick={() => dispatch(modalAction.hideModal())}
              >
                Hủy bỏ
              </Button>
            </Box>
            <Box ml={1}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => handleClickTask(task)}
              >
                Xóa
              </Button>
            </Box>
          </Box>
        </div>
      )
    );
  };
  const handleClickTask = (task) => {
    const { id } = task;
    console.log('task ', task);
    console.log('id ', id);
    dispatch(taskAction.deleteTask(id));
  };
  const handleOpen = () => {
    dispatch(modalAction.showModal());
    dispatch(modalAction.changeModalTitle("Thêm mới công việc"));
    dispatch(modalAction.changeModalContent(<TaskForm />));
    //Xét để đưa nd task đang chỉnh sửa về null
    dispatch(taskAction.setTaskEditing(null));
  };

  const renderSearchBox = () => {
    let xhtml = null;
    xhtml = <SearchBox handleChange={handleFilter} />;
    return xhtml;
  };
  const handleFilter = (e) => {
    e.preventDefault();
    const { value } = e.target;
    dispatch(filterTask(value));
  };

  return (
    <div className="custome_width">
      <Button variant="contained" color="primary" onClick={handleOpen}>
        <AddIcon /> Thêm công việc mới
      </Button>
      {renderSearchBox()}
      {renderBoard()}
    </div>
  );
}

export default TaskBoard;

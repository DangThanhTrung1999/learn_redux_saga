import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Switch, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AdminLayoutRoute from "../../commons/Layout/AdminLayoutRote";
import GlobalLoading from "../../component/GloabalLoading";
import MyModal from "../../component/MyModal";
import { ADMIN_ROUTES, ROUTES } from "../../constants";
import configureStore from "../../redux/configureStore";
import DefaultLayoutRote from "../../commons/Layout/DefaultLayoutRote";
const useStyles = makeStyles({
  box: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
  },
});
const store = configureStore();
const renderAdminRoute = () => {
  let xhtml = ADMIN_ROUTES.map((route) => {
    return (
      <AdminLayoutRoute
        key={route.path}
        path={route.path}
        component={route.component}
        exact={route.exact}
        name={route.name}
      />
    );
  });
  return xhtml;
};
const renderDefaultRoute=()=>{
  let xhtml = ROUTES.map((route) => {
    return (
      <DefaultLayoutRote
        key={route.path}
        path={route.path}
        component={route.component}
        exact={route.exact}
        name={route.name}
      />
    );
  });
  return xhtml
}
function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          {/* <div className={classes.box}>
            <div className={classes.shape}>ReactJs</div>
            <div className={classes.shape}>AngularJs</div>
          </div> */}

          <ToastContainer />
          <GlobalLoading />
          <MyModal />
          <Switch>
            <Redirect exact from="/" to="/admin" />
            {renderAdminRoute()}
            {renderDefaultRoute()}
          </Switch>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

import React from "react";
import { Route } from "react-router-dom";
function DefaultLayoutRote(props) {
  const { component: YourComponent, ...remainProps } = props;
  return (
    <Route
      {...remainProps}
      render={(routeProps) => {
        return <YourComponent {...routeProps} />;
      }}
    />
  );
}
export default DefaultLayoutRote;

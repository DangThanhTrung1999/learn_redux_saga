import React from "react";
import { useSelector } from "react-redux";
import LoadingIcon from "./../../assets/image/loading.gif";
import "./styles.css";
function GlobalLoading(props) {
  const showLoading = useSelector((state) => state.ui.showLoading);
  if (showLoading)
    return (
      <div className="globalLoading">
        <img src={LoadingIcon} alt="loading" className="icon" />
      </div>
    );
  return null;
}

export default GlobalLoading;

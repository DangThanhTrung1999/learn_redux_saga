import React from "react";
import {
  Grid,
  Card,
  Typography,
  CardActions,
  CardContent,
  Fab,
  Icon,
} from "@material-ui/core";
import { STATUS } from "../../constants";
function TaskItem(props) {
  const { status, description, title } = props.task;
  return (
    <Card
      style={{
        marginBottom: 20,
      }}
    >
      <CardContent>
        <Grid container justify="space-between">
          <Grid item md={8}>
            <Typography component="h2">{title}</Typography>
          </Grid>
          <Grid item md={4}>
            {STATUS[status].label}
          </Grid>
        </Grid>
        <p>{description}</p>
      </CardContent>
      <CardActions>
        <Fab
          color="primary"
          aria-label="Edit"
          size="small"
          onClick={props.handleEditTask}
        >
          <Icon fontSize="small">edit_icon</Icon>
        </Fab>
        <Fab color="secondary" aria-label="Edit" size="small"
        onClick={props.showModalDeleteTask}>
          <Icon fontSize="small">delete_icon</Icon>
        </Fab>
      </CardActions>
    </Card>
  );
}

export default TaskItem;

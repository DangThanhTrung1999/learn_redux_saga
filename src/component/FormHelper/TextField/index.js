import React from 'react';
import { TextField } from "@material-ui/core";

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <TextField
    label={label}
    placeholder={custom.placeholder}
    fullWidth
    InputLabelProps={{ 
      shrink: true,
    }}
    variant="outlined"
    error={touched && invalid}
    helperText={touched && error}
    {...input}
    {...custom}
  />
);
export default renderTextField;

import React from "react";
import { TextField } from "@material-ui/core";

function SearchBox(props) {
  return (
    <form noValidate autoComplete="off">
      <TextField
      autoComplete="off"
        id="standard-name"
        label="Name"
        type="text"
        onChange={props.handleChange}
        margin='normal'
      />
    </form>
  );
}

export default SearchBox;

import React from "react";
import { Modal, Button } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { useSelector, useDispatch } from "react-redux";
import * as modalAction from "../../actions/modal";
import "./styles.css";
function MyModal(props) {
  const state = useSelector((state) => state.modal);
  // console.log(state);
  const { title, showModal, component } = state;
  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(modalAction.hideModal());
  };
  return (
    <Modal open={showModal} onClose={handleClose}>
      <div className="modal">
        <div className="header">
          <h4>{title}</h4>
          <Button onClick={handleClose}>
            <CloseIcon />
          </Button>
        </div>
        {component}
      </div>
    </Modal>
  );
}

export default MyModal;

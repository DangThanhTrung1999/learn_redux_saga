import React from "react";
import { Grid, Box } from "@material-ui/core";

import TaskItem from "../TaskItem";
function TaskList(props) {
  let { status, listTaskFiltered, handleEditTask, showModalDeleteTask } = props;
  return (
    <Grid key={status.value} item md={4} xs={12}>
      <Box mb={2} mt={2}>
        <div style={{ fontWeight: "bold" }}>{status.label}</div>
      </Box>
      <div>
        {listTaskFiltered.map((task) => {
          return (
            <TaskItem
              task={task}
              key={task.id}
              handleEditTask={() => handleEditTask(task)}
              showModalDeleteTask={() => showModalDeleteTask(task)}
            />
          );
        })}
      </div>
    </Grid>
  );
}

export default TaskList;

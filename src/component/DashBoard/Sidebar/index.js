import { makeStyles } from "@material-ui/core";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import * as uiAction from "../../../actions/ui";
import { ADMIN_ROUTES } from "../../../constants/index";
const useStyles = makeStyles({
  drawerPaper: {
    width: 240,
    zIndex: 10,
    maxWidth: 240,
    height: "100%",
    position: "relative",
  },
  menuLink: {
    textDecoration: "none",
    color: "black",
  },
  menuLinkActive: {
    "&>div": {
      backgroundColor: "#0019ff1f",
    },
  },
});
function Sidebar(props) {
  const classes = useStyles();
  const renderList = () => {
    let xhtml = null;
    xhtml = (
      <div className="sasd">
        <List component="div">
          {ADMIN_ROUTES.map((item) => {
            return (
              <NavLink
                to={item.path}
                exact={item.exact}
                className={classes.menuLink}
                activeClassName={classes.menuLinkActive}
                key={item.path}
              >
                <ListItem className="menuItem" button>
                  {item.name}
                </ListItem>
              </NavLink>
            );
          })}
        </List>
      </div>
    );
    return xhtml;
  };
  const dispatch = useDispatch();
  return (
    <Drawer
      //   anchor="left"
      open={props.showSidebar}
      onClose={() => dispatch(uiAction.hideSidebar())}
      classes={{ paper: classes.drawerPaper }}
      variant="persistent"
    >
      {renderList()}
    </Drawer>
  );
}

export default Sidebar;

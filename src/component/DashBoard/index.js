import { makeStyles } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import Header from "./Header";
import Sidebar from "./Sidebar";
import cn from "classnames";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "row",
    height: "100%",
  },
  wrapperContent: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  shiftLeft: {
    marginLeft: -240,
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
}));
function Dashboard(props) {
  const { children, name } = props;

  const showSidebar = useSelector((state) => state.ui.showSidebar);
  const classes = useStyles();
  return (
    <div>
      <Header name={name} />
      <div className={classes.wrapper}>
        <Sidebar className={classes.sidebar} showSidebar={showSidebar} />
        <div
          className={cn(classes.wrapperContent, {
            [classes.shiftLeft]: showSidebar === false,
          })}
        >
          {children}
        </div>
      </div>
    </div>
  );
}

export default Dashboard;

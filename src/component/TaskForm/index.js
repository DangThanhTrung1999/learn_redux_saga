import { Box, Button, Grid } from "@material-ui/core";
import React from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { Field, reduxForm } from "redux-form";
import * as modalAction from "../../actions/modal";
import * as taskAction from "../../actions/task";
import renderTextField from "../../component/FormHelper/TextField";
import renderSelectField from "../../component/FormHelper/SelectField";
import "./styles.css";
import { STATUS } from "../../constants/index";
const validate = (values) => {
  const errors = {};
  if (!values.title || values.title.trim() === "") {
    errors.title = "Vui lòng nhập tiêu đề";
  } else if (values.title.length < 5 || values.title.trim().length < 5) {
    errors.title = "Ít nhất 5 kí tự trở lên";
  }
  if (!values.description || values.description.trim() === "") {
    errors.description = "Vui lòng nhập mô tả";
  }
  if (!values.status) {
    errors.status = "Vui lòng chọn trạng thái";
  }
  return errors;
};

const FORM_NAME = "TASK_MANAGER";

let TaskForm = (props) => {
  const dispatch = useDispatch();
  const taskEditing = useSelector((state) => state.task.taskEditing);
  const { handleSubmit } = props;
  const handleClose = () => {
    dispatch(modalAction.hideModal());
  };
  const handleFormSubmit = (data) => {
    const { title, description, status } = data;
    if (taskEditing && taskEditing.id) {
      dispatch(taskAction.updateTask(title, description, parseInt(status)));
    } else {
      dispatch(taskAction.addTask(title, description));
    }
  };

  const { invalid, submitting } = props;

  const renderStatus = () => {
    let xhtml = null;
    if (taskEditing && taskEditing.id) {
      xhtml = (
        <Grid item md={12} className="pdt-0" style={{width:'100%'}}>
          <Field
            id="status"
            label="Status"
            placeholder="Status"
            margin="normal"
            name="status"
            component={renderSelectField}
          >
            <option value="" />
            <option value={0}>Ready</option>
            <option value={1}>In progress</option>
            <option value={2}>Complete</option>
          </Field>
        </Grid>
      );
    }
    return xhtml;
  };
  return (
    <form onSubmit={handleSubmit(handleFormSubmit)}>
      <Grid container className="custome" >
        <Grid item md={12} className="pdt-0"  style={{width:'100%'}}>
          <Field
            id="title"
            label="Tiêu đề"
            placeholder="Tiêu đề"
            margin="normal"
            name="title"
            component={renderTextField}
          />
        </Grid>
        <Grid item md={12} className="pdt-0"  style={{width:'100%'}}>
          <Field
            id="description"
            label="Mô tả"
            placeholder="Mô tả"
            margin="normal"
            name="description"
            component={renderTextField}
          />
        </Grid>
        {renderStatus && renderStatus()}
        <Grid item md={12} className="pdt-0">
          <Box display="flex" flexDirection="row-reverse">
            <Button
              disabled={invalid || submitting}
              color="primary"
              type="submit"
            >
              Lưu lại
            </Button>
            <Button color="secondary" onClick={handleClose}>
              Hủy bỏ
            </Button>
          </Box>
        </Grid>
      </Grid>
    </form>
  );
};

TaskForm = reduxForm({
  // a unique name for the form
  form: FORM_NAME,
  validate,
})(TaskForm);

TaskForm = connect((state) => ({
  initialValues: state.task.taskEditing,
}))(TaskForm);
export default TaskForm;

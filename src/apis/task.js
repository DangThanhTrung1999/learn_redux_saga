import axiosService from "../commons/axiosService";
import { API } from "../constants";
import queryString from "query-string";

const url = "tasks";
export const getList = (params = {}) => {
  let queryParams = "";
  if (Object.keys(params).length > 0) {
    queryParams = `?${queryString.stringify(params)}`;
  }
  return axiosService.get(`${API}/${url}${queryParams}`);
};
export const addTask = (data) => {
  return axiosService.post(`${API}/${url}`, data);
};
export const updateTask = (data, taskId) => {
  console.log(data);
  return axiosService.put(`${API}/${url}/${taskId}`, data);
};
export const deleteTask = (taskId) => {
  return axiosService.delete(`${API}/${url}/${taskId}`);
};



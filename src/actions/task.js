import * as taskApi from "./../apis/task";
import * as taskConstants from "./../constants/task";
export const fetchListTask = (params = {}) => {
  return {
    type: taskConstants.FETCH_TASK,
    payload: {
      params,
    },
  };
};
export const fetchListTaskSuccess = (data) => {
  return {
    type: taskConstants.FETCH_TASK_SUCCESS,
    payload: { data },
  };
};
export const fetchListTaskFailed = (error) => {
  return {
    type: taskConstants.FETCH_TASK_FAILED,
    payload: {
      error,
    },
  };
};

export const addTask = (title, description) => {
  return {
    type: taskConstants.ADD_TASK,
    payload: {
      title,
      description,
    },
  };
};
export const addTaskSuccess = (data) => {
  return {
    type: taskConstants.ADD_TASK_SUCCESS,
    payload: { data },
  };
};
export const addTaskFailed = (error) => {
  return {
    type: taskConstants.ADD_TASK_FAILED,
    payload: {
      error,
    },
  };
};

export const fetchListTaskApi = () => {
  return (dispatch) => {
    dispatch(fetchListTask());
    taskApi
      .getList()
      .then((res) => {
        const { data } = res;
        dispatch(fetchListTaskSuccess(data));
      })
      .catch((error) => dispatch(fetchListTaskFailed(error)));
  };
};

export const filterTask = (keyWord) => ({
  type: taskConstants.FILTER_TASK,
  payload: {
    keyWord,
  },
});

export const filterTaskSuccess = (data) => ({
  type: taskConstants.FILTER_TASK_SUCCESS,
  payload: {
    data,
  },
});
export const setTaskEditing = (task) => ({
  type: taskConstants.SET_TASK_EDITING,
  payload: {
    task,
  },
});

export const updateTask = (title, description, status) => {
  return {
    type: taskConstants.UPDATE_TASK,
    payload: {
      title,
      description,
      status,
    },
  };
};
export const updateTaskSuccess = (data) => {
  return {
    type: taskConstants.UPDATE_TASK_SUCCESS,
    payload: { data },
  };
};
export const updateTaskFailed = (error) => {
  return {
    type: taskConstants.UPDATE_TASK_FAILED,
    payload: {
      error,
    },
  };
};
export const deleteTask = (id) => {
  return {
    type: taskConstants.DELETE_TASK,
    payload: {
      id,
    },
  };
};
export const deleteTaskSuccess = (id) => {
  return {
    type: taskConstants.DELETE_TASK_SUCCESS,
    payload: { id },
  };
};
export const deleteTaskFailed = (error) => {
  return {
    type: taskConstants.DELETE_TASK_FAILED,
    payload: {
      error,
    },
  };
};
